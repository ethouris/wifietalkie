# WifieTalkie

An application that allows users to communicate in a push-to-talk style using mobile phone, or any other device, with the use of local network.

# Basic statements

## Concept

The motivation for this application is an ability to use mobile phones as a push-to-talk radio communicator within one local network
(although extensions that could use agents to carry out the transmission across networks might be also possible). In general, this
application should allow a user of the device to send a voice message to another device, with recognizing particular party as a user
by its name in the application, and the message should be played by the receiveing user's application live as the sending user
is speaking with minimum possible latency.

## Contents

For communication it's predicted to use the SRT library in order to implement connections and network transmission. Beside this,
however, for recognition and user broadcasting there's a multicast network (IGMP groups) used so that there's no need for any
overall system, registration, or any kind of central database.

The overall system should consist of the following components:

### 1. User registration
--------------------

Users must be unique throughout the network. It should be allowed that new users register with their new code ID, however they need
to use a newly generated hash code for that purpose, and then hide that code in an encrypted file. This is important so that this
code can be reused when the same user would like to register themself with a new device. In total a new user should have:
 - public code, which is generated basing on the private code, and it's published over the network
 - private code, which should be hidden secretly
 - user name

There should be a system able to detect a fraud public code.

### 2. User broadcasting
--------------------

For user broadcasting there's needed a local multicast group. The router of the local network should be capable of using IGMP groups,
as well as all communication devices should have multicast network enabled.

Whenever a user connects to a local network, each must have some unique identifier obtained so that every network can be uniquely
identified. Every network may have its own unique group IP address, although the default IP address for this application is:
225.225.22.55, port: 2255. This IP address should be able to be tried when no other is used. If a local network administrator has
disallowed this address from being used, some other IP address must be agreed upon for that network. The unique identification of
the network is required so that it's known whether this IP address can be used for multicast or some other must be configured.

This multicast address is used for broadcasting a new user in the network. Whenever a device connects to a local network, the
application should announce this user by sending packets with this information as to who has connected. All existing users in the
network should listen on that multicast address and receive this information, then update it in their own databases. Unique public
codes are required so that users are matched with the users registered in the addressbook and show availability of particular users.

The following information is published by a registering user:
 - public code
 - user name
 - IP address and port where the user is listening (the user's device), default port is 2255

When a new user is registering in the network, existing users should respond with a repeated announcement. This is required for
the new user to update the existing user database.


### 3. Communication
----------------

The application should allow the user to pass a direct voice message to a given user as identified in the user database.
It is expected that the user interface:
 - allow to select a user to send a message to
 - after pressing a button, a status should inform as to whether the user is contacted
 - the voice is being recorded and passed on the fly as long as the status is reported as connected
 - the voice passed to the application when the connection isn't ready should be dropped
 - the button informs the user with the color as to whether it is recording the message right now, or is not ready
(recommended green and red, respectively)

At the moment when a user requests to send a message, the SRT connection should be made to the IP and port as declared
by particular user. The user should accept the connection, and immediately thereafter read the voice data from the
connection and play it as it goes. SRT should work in a default live mode for this connection. When the sender user
releases the push-to-talk button in the application, the connection should be closed once the message recorded up to
the moment of releasing the button has been sent to all recipients.

An encryption of the connection should be possible by using a pre-shared passphrase. This should be configured in the user
database. This passphrase can be then used in every case of making a connection. The passphrase must be entered manually
on every device and no communication means should be used to pass this passphrase other than encrypted ones.

Users may also want to establish a longer connection for speaking to have free hands - the UI should also offer a
possibility to lock the "push-to-talk" button.

